
<?php

class Homework
{
    function reverString($str)
    {
        $str . " "; // to acept the last work 
        $strTemp = "";
        $stsrRetrun = "";
        for ($j = 0; $j < strlen($str); $j++) {

            $strTemp .=  $str[$j]; // get each char of string and concate those string 

            if (strcmp($str[$j], " ") == 0) { // if it have space we need to reverse this string 
                $stsrRetrun = $stsrRetrun . strrev($strTemp); // this will be conate to those string that have reverse 
                $strTemp = ""; // afer rever clear string 
            }
        }
        return $stsrRetrun;
    }

    function filterEvenNum(array $num)
    {
        $arrTemp = array();
        for ($i = 0; $i <  count($num); $i++) {

            if ($num[$i] % 2 == 0) {

                $arrTemp[] = $num[$i];
            }
        }
        return $arrTemp;
    }
   
    function sumInfitNum(...$nums)
    {
        $sum = 0;
        foreach ($nums as $n) {
            $sum += $n;
        }
        return $sum;
    }
}

$homework = new Homework();

echo "-----------------Reverse String-----------------\n";

echo $homework->reverString("emocleW ot PHP ") . "\n";


echo "-----------------Find even number-----------------\n";
$evenNum =  $homework->filterEvenNum((array(2, 3, 4, 6, 7, 9, 11, 20)));
for ($i = 0; $i < count($evenNum); $i++) {
    echo "event number : " . $evenNum[$i] . "\n";
}

$nums = fn ($a) => $homework->filterEvenNum($a);
$evenNums =  $nums(array(2, 3, 4, 6, 7, 9, 11, 20));
for ($i = 0; $i < count($evenNums); $i++) {
    echo "even number : " . $evenNums[$i] . ", ";
}


echo "----------------- Sum number ----------------- \n";
echo "sum number " . $homework->sumInfitNum(2, 3) . "\n";
echo "sum number " . $homework->sumInfitNum(2, 3, 4) . "\n";
echo "sum number " . $homework->sumInfitNum(2, 3, 4, 5) . "\n";
?>
