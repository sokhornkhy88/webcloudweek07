<?php
require_once(__DIR__ . '/../factory2/PaymentFactory.php');
require_once(__DIR__ . '/../app/productModel.php');
class App
{
    function buyProduct($itemName, $price, $qty, $itemSelectedPayBy)
    {
        $paymentMethod = new PaymentFactory();

        $userPayBy = $paymentMethod->getpaymentMethod($itemSelectedPayBy);
        $payWith =  $userPayBy->pay();

        $productModel = new ProductModel($itemName, $price, $qty, $payWith);
        $totalPrice = $productModel->getTotalAmount();

        echo "Name: " . $itemName . " price: " . $price . " qty: "  . $qty . " pay with: " . $payWith . " Total: " . $totalPrice . "\n";
        return $totalPrice;
    }
}
$app = new App();
$app->buyProduct("item1", 12, 10, "ABA");
$app->buyProduct("item2", 12, 5, "WING");
$app->buyProduct("item3", 12, 1, "PIPAY");
