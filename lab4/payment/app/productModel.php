<?php
class ProductModel
{
    private $itemName;
    private $price;
    private $qty;
    private $paymentMethod;
    private $total;

    function __construct($itemName, $price, $qty, $paymentMethod)
    {
        $this->$itemName  = $itemName;
        $this->price = $price;
        $this->qty = $qty;
        $this->$paymentMethod  = $paymentMethod;
    }

    function getTotalAmount()
    {
        return  $this->price * $this->qty;
    }
}
