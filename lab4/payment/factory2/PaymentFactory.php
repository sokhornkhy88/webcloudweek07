<?php
require_once(__DIR__ . '/pay/ABA.php');
require_once(__DIR__ . '/pay/Pipay.php');
require_once(__DIR__ . '/pay/Wing.php');
class PaymentFactory
{
    public function getpaymentMethod($payBy)
    {
        $payBy = strtoupper($payBy);
        if ($payBy == "ABA") {
            return new ABA();
        } else if ($payBy == "WING") {
            return new Wing();
        } else if ($payBy == "PIPAY") {
            return new Pipay();
        }
        return null;
    }
}
