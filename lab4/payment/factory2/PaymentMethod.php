<?php
abstract class PaymentMethod
{
    abstract function pay();
}
